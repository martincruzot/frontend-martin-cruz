$(document).ready(function(){

    $('#formulario')
        .bootstrapValidator({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                first_name: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Los nombres son requeridos'
                        }
                    }
                },
                last_name: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Los apellidos son requeridos'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'El número de teléfono es requerido'
                        },
                        regexp: {
                            message: 'El numero de teléfono no debe contener digitos, espacios, -, (, ), + ni .',
                            regexp: /^[0-9\s\-()+\.]+$/
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'El email es requerido'
                        },
                        emailAddress: {
                            message: 'Ingreso un email inválido'
                        }
                    }
                },
                address: {
                    validators: {
                        notEmpty: {
                            message: 'La dirección es requerida'
                        }
                    }
                }
            }
        });
});
